<?php

function pfts_migrate_form($form, &$form_state) {

  // Warn the user.

  $form['warning'] = array(
    '#markup' => t('Please note that migrating your field data is an operation that is likely to fail and your should not begin it without a backup of your data. Depending on how much field data you have this process may take seconds, minutes or even hours. You have been warned!'),
  );

  // Show the fields that will get migrated.
  $fields = field_read_fields(array('storage_type' => 'field_sql_storage'), array('include_inactive' => TRUE));
  $new_groups = array();

  foreach ($fields as $field) {
    $new_table = _pfts_tablename($field);
    $new_groups[$new_table][] = array(check_plain($field['field_name']), check_plain($field['type']));
  }

  $form['fields'] = array(
    '#type' => 'fieldset',
    '#title' => t('Fields to migrate'),
    '#description' => t('These tables show the fields that will be migrated, and the groupings of those fields.'),
  );

  $count = 1;
  foreach ($new_groups as $new_table => $rows) {
    $form['fields'][$new_table] = array(
      '#theme' => 'table',
      '#header' => array(t('Field name'), t('Field type')),
      '#rows' => $rows,
      '#caption' => t('Group @count', array('@count' => $count++))
    );
  }

  // Offer a conversion.
  $form['actions'] = array('#type' => 'actions');

  $form['confirmation'] = array(
    '#type' => 'checkbox',
    '#default_value' => FALSE,
    '#required' => TRUE,
    '#title' => t('I confim that I understand the consequences of this action.'),
  );

  $form['actions']['convert'] = array(
    '#type' => 'submit',
    '#value' => t('Convert to per field-type storage'),
    '#states' => array(
      'enabled' => array(
        ':input[name="confirmation"]' => array('checked' => TRUE),
      ),
    ),

  );


  return $form;
}

function pfts_migrate_form_submit($form, &$form_state) {
  // Going to convert all the 'old' fields tables to 'new' ones.
  // This might take a while, so lets do it in a batch.

  $batch = array(
    'file' => drupal_get_path('module', 'pfts_migrate') . '/pfts_migrate.admin.inc',
    'operations' => array(),
  );

  // Get a list of all fields.
  $fields = field_read_fields(array('storage_type' => 'field_sql_storage'), array('include_inactive' => TRUE));

  foreach ($fields as $field) {
    $batch['operations'][] = array(
      'pfts_migrate_form_submit_batch',
      array($field),
    );
  }

  $batch['finished'] = 'pfts_migrate_form_submit_batch_finished';

  batch_set($batch);

}

function pfts_migrate_form_submit_batch($prior_field, &$context) {
  $context['message'] = t('Processing @field_name', array('@field_name' => $prior_field['field_name']));

  // Set up our phases.
  if (!isset($context['sandbox']['phase'])) {
    $context['sandbox']['phase'] = 'create_tables';
  }

  switch ($context['sandbox']['phase']) {
    case 'create_tables':
      // We need to create the tables for the new field.
      $field = $prior_field;
      $field['storage_type'] = 'pfts';
      $field['storage_module'] = 'pfts';
      $field['storage']['type'] = 'pfts';
      $field['storage']['module'] = 'pfts';
      $data = $field;
      unset($data['columns'], $data['field_name'], $data['type'], $data['locked'], $data['module'], $data['cardinality'], $data['active'], $data['deleted']);
      $field['data'] = $data;
      drupal_write_record('field_config', $field, array('id'));
      // Clear caches
      field_cache_clear(TRUE);

      pfts_field_storage_create_field($field);

      // Advance to the next phase.
      $context['sandbox']['phase'] = 'migrate_data';

      // Make sure we're called again.
      $context['finished'] = 0.1;
      break;

    case 'migrate_data':
      // Migrate the data using a hairy DB query.
      $field = field_read_field($prior_field['field_name'], array('include_inactive' => TRUE));
      // Handle the data tables first.
      $old_table = _field_sql_storage_tablename($prior_field);
      $new_table = _pfts_tablename($field);
      // Create an array mapping columns in the old tables -> columns in the new tables.
      $db_column_mapping = array(
        'entity_type' => 'entity_type',
        'bundle' => 'bundle',
        'entity_id' => 'entity_id',
        'revision_id' => 'revision_id',
        'deleted' => 'deleted',
        'delta' => 'delta',
        'language' => 'language',
      );
      foreach ($prior_field['columns'] as $name => $spec) {
        $db_column_mapping[_field_sql_storage_columnname($prior_field['field_name'], $name)] = _pfts_columnname($prior_field['field_name'], $name);
      }
      $select = db_select($old_table);
      $select->addExpression("'" . $prior_field['field_name'] . "'", 'field_name');
      // Add all the other fields as expressions too.
      foreach (array_keys($db_column_mapping) as $column) {
        $select->addExpression("$old_table.$column", $column);
      }
      // Now build the insert query.
      $insert = db_insert($new_table);
      $insert->fields(array_merge(array('field_name'), array_values($db_column_mapping)));
      $insert->from($select);

      // Run the insert.
      // @TODO batch and select and insert 10,000 rows at a time.
      $insert->execute();

      // Advance to the next phase.
      $context['sandbox']['phase'] = 'migrate_revision';

      // Make sure we're called again.
      $context['finished'] = 0.5;
      break;

    case 'migrate_revision':
      // Migrate the data using a hairy DB query.
      $field = field_read_field($prior_field['field_name'], array('include_inactive' => TRUE));
      // Handle the data tables first.
      $old_table = _field_sql_storage_revision_tablename($prior_field);
      $new_table = _pfts_revision_tablename($field);
      // Create an array mapping columns in the old tables -> columns in the new tables.
      $db_column_mapping = array(
        'entity_type' => 'entity_type',
        'bundle' => 'bundle',
        'entity_id' => 'entity_id',
        'revision_id' => 'revision_id',
        'deleted' => 'deleted',
        'delta' => 'delta',
        'language' => 'language',
      );
      foreach ($prior_field['columns'] as $name => $spec) {
        $db_column_mapping[_field_sql_storage_columnname($prior_field['field_name'], $name)] = _pfts_columnname($prior_field['field_name'], $name);
      }
      $select = db_select($old_table);
      $select->addExpression("'" . $prior_field['field_name'] . "'", 'field_name');
      // Add all the other fields as expressions too.
      foreach (array_keys($db_column_mapping) as $column) {
        $select->addExpression("$old_table.$column", $column);
      }
      // Now build the insert query.
      $insert = db_insert($new_table);
      $insert->fields(array_merge(array('field_name'), array_values($db_column_mapping)));
      $insert->from($select);

      // Run the insert.
      // @TODO batch and select and insert 10,000 rows at a time.
      $insert->execute();

      // Advance to the next phase.
      $context['sandbox']['phase'] = 'drop_tables';

      // Make sure we're called again.
      $context['finished'] = 0.9;
      break;

    case 'drop_tables':
      // Remove the old DB tables.
      $table = _field_sql_storage_tablename($prior_field);
      db_drop_table($table);
      $table = _field_sql_storage_revision_tablename($prior_field);
      db_drop_table($table);
      $context['results']['status'][] = t('Migrated data for @title', array('@title' => $prior_field['field_name']));
      break;
  }


}

/**
 * Finish callback for the Migrate import batch.
 */
function pfts_migrate_form_submit_batch_finished($success, $results, $operations) {
  if ($success) {
    foreach (array('error', 'status') as $type) {
      if (isset($results[$type])) {
        foreach ($results[$type] as $message) {
          drupal_set_message($message, $type);
        }
      }
    }
  }
}